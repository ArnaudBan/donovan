---
title: 1er aventure avec les enfants
date: 2021-05-09T13:25:17.094Z
author: ArnaudBan
description: 1er aventure avec les enfants, de guérande à Piriac
tags:
  - piriac-2021
layout: layouts/post.njk
---

Fini le confinement et la régle des déplacements de moins de 10 km. Fini les températures hivernale. A nous l'aventure !

Ma collégue Pauline a eu la gentillesse de me faire une liste des trucs cool a voir autour de Piriac, ville où elle a grandit. Avec ces préciseuses informations nous prenons la route le vendredi soir aprés le travail et l'école.

## Guérande

On est arrivé le vendredi soir a Guérande, on a commandé un pizza et en attendant qu'ils nous la prépare on a été voir les remparts !

![/img/1er-avanture-guerande-muraile.jpg](/img/1er-avanture-guerande-muraile.jpg)

*Muraille de Guérande*

![/img/1er-avanture-guerande-muraile-4.jpg](/img/1er-avanture-guerande-muraile-4.jpg)

*Porte Saint Michel de Guérande*

On a mangé et dormis dans le van dans une aire de camping car gratuite pas loin du centre du Guérande. Pratique mais pas super sympatique.

![/img/1er-avanture-guerande-pizza.jpg](/img/1er-avanture-guerande-pizza.jpg)

*Miam de la pizza*

![/img/1er-avanture-guerande-mediation.jpg](/img/1er-avanture-guerande-mediation.jpg)

*Étirements post médiation*

![/img/1er-avanture-guerande-c-le-pied.jpg](/img/1er-avanture-guerande-c-le-pied.jpg)

*La brioche au petit dej c'est le pied*

![/img/1er-avanture-guerande-monument-2.jpg](/img/1er-avanture-guerande-monument-2.jpg)

On a visité Guérande. J'ai bien aimé le centre ville. ( Elijah )

## Piriac

Aprés avoir fait le marché à Guérande on a mis le cap sur la plage de Pauline aussi appelé Plade de Belmont.

![/img/1er-avanture-plage-de-belmont.jpg](/img/1er-avanture-plage-de-belmont.jpg)
![/img/1er-avanture-plage-de-belmont-2.jpg](/img/1er-avanture-plage-de-belmont-2.jpg)

Grosse session de jeux dans les rochers avant le repas de midi.

![/img/1er-avanture-plage-de-belmont-3.jpg](/img/1er-avanture-plage-de-belmont-3.jpg)
![/img/1er-avanture-plage-de-belmont-4.jpg](/img/1er-avanture-plage-de-belmont-4.jpg)
![/img/1er-avanture-plage-de-belmont-6.jpg](/img/1er-avanture-plage-de-belmont-6.jpg)

J'ai bien aimé monter sur les rochers. (Elijah)

![/img/1er-avanture-paela.jpg](/img/1er-avanture-paela.jpg)
![/img/1er-avanture-plage-de-belmont-netoyage.jpg](/img/1er-avanture-plage-de-belmont-netoyage.jpg)

Aprés un repas a base de paela c'est l'heure d'une sieste sur la plage pour moi pendant que les enfants creusais un trou. Puis on a repris l'aventure. Les enfants ont netoyé le van et on est parti marcher pour découvrir la point du Casteli

![/img/1er-avanture-pointe-du-casteli.jpg](/img/1er-avanture-pointe-du-casteli.jpg)

*Cémaphore de la pointe du Casteli*

![/img/1er-avanture-pointe-du-casteli-4.jpg](/img/1er-avanture-pointe-du-casteli-4.jpg)

*Magnifique côte menant a la pointe*

![/img/1er-avanture-pointe-du-casteli-5.jpg](/img/1er-avanture-pointe-du-casteli-5.jpg)

*Arche*

![/img/1er-avanture-pointe-du-casteli-6.jpg](/img/1er-avanture-pointe-du-casteli-6.jpg)

*Côte en rentrant vers le van*

## Port de Piriac

Aprés cette petite promenade on commencait a avoir faim ! Direction de port de Piriac ou on a eu le bonheur de trouvé de bonne glaces. Miam.

![/img/1er-avanture-piriac-glace.jpg](/img/1er-avanture-piriac-glace.jpg)

*Glace italienne vanille fraise pour ELijah*

![/img/1er-avanture-piriac-glace-j.jpg](/img/1er-avanture-piriac-glace-j.jpg)

*Sorbet mange pour Joakim*

![/img/1er-avanture-piriac.jpg](/img/1er-avanture-piriac.jpg)

*Déambulation dans les petite rue pitoresque de Piriac*
