---
title: Fin du confinement en Normandie
description: Départ de Nantes pour la normandie avec Donovan
date: 2020-12-14
tags:
  - normandie-2020
layout: layouts/post.njk
---

J'ai quitter Nantes ce matin à 8h en direction de Chateaubriand. La destination que je vise est Coutance en Normandie. J'ai choisir cette destination un des dernier livre que j'ai lu ce passé là bas, Sur la route de ses rêves de Marie-Laure Bigand. Le livre était pas ouf mais la région était bien décrite et donné envie. Autre critére important la météo, je cherche a éviter la pluie. Aujourd'hui il a plu toute la journée normalement demain ca devrait être mieux d'aprés la mété.

Tous les lundi a 9h30 j'ai réunions d'équipe et en regardant la carte j'ai vu que Chateaubriand serait la destination que j'attendrais à 9h30 et j'ai donc prévu d'y travailler la matiné et d'en profiter le midi pour manger et visiter le chateau.

En fait je me suis arrêté juste avant Chateaubriand car il était l'heure. J'ai donc fait ma réunion par téléphone avec Meet, le service de visio conférence de Google, sur les bords d'un petit étang ou joué des ragondin à Moisdon la rivière. Juste aprés ma réunions j'ai fini la route et j'ai travailler toute la matiné sur le parking devant le chateau de Chateaubriand.

![/img/87138BBB-84D4-43CA-9977-D2A0061A4656.jpeg](/img/87138BBB-84D4-43CA-9977-D2A0061A4656.jpeg)

En pleine réunion d’équipe

![Réunion d'équipe](/img/E806662D-7124-44C1-837F-A9157D7C8B02.jpeg)

Des ragondins très heureux

A midi j'ai marché un peu dans le centre et je me suis promenais dans le jardin du chateau qui était encore ouvert malgrés la pandémie de Covid.

![/img/7F5B6CF0-B1E2-4810-A453-1B46CB5D64B1.jpeg](/img/7F5B6CF0-B1E2-4810-A453-1B46CB5D64B1.jpeg)

Section renaissance du château de Chateaubriand

![/img/0D7BF581-5060-477D-A943-4F7F6F193AE4.jpeg](/img/0D7BF581-5060-477D-A943-4F7F6F193AE4.jpeg)

![/img/AF0886C9-AC3E-4CA1-A5E9-949DA869DC16.jpeg](/img/AF0886C9-AC3E-4CA1-A5E9-949DA869DC16.jpeg)

![/img/5E5E76D6-ED55-484F-BA93-E8641C419680.jpeg](/img/5E5E76D6-ED55-484F-BA93-E8641C419680.jpeg)

En haut des remparts médiévaux

Ensuite j'ai repris la route avec comme objectif un parking avec prise électrique gratuite dans une ville pas trés loin appeler Retiers. Malheureusement la borne était horse service. Par contre j'ai pu rechargé mon ordinateur portable avec l'allume cigare de la batterie moteur sans soucis. J'ai donc travailler là toute l'aprés midi. A 18h00 j'ai repris la route direction la Normandie !

Je suis arrivé à 19h30 sur la côte a côté de Granville. Je dors ce soir face à la mer. Mais je n'ai encore rien vu car il fait nuit et il pleu. Aprés avoir manger mes pâtes et un bon panetone. J'ai retravailler un peu pour Clovis et son site internet Puzzle Vidéo.

Demain matin je pourrais me ballader sur la plage si tout va bien et ce soir je vais m'endormir au son des vagues.

Aujourd'hui c'est le dernier jours du confinement. Demain on aura plus besoin d'attestation pour ce déplacer dehors mais par contre il y aura un couvre feu de 20h à 6h.

J'ai repéré sur internet un endroit de coworking a Coutance. J'irais voir demain.
