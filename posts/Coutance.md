---
title: De Donville-les-Bains à Agon-Coutainville
description: A la recherche du bon spot
date: 2020-12-15
author: ArnaudBan
tags:
  - normandie-2020
layout: layouts/post.njk
---

Je continue de me promenais en Normandie. Je cherche trois chose pour qu'un emplacement soit valide :
* Une météo la plus clémente possible (si possible pas de pluie surtout)
* De la 4G car j'ai besoin d'un accés internet correct pour travaillé
* Une prise electrique sur laquelle me branché

C'est ce dernier point qui m'a fait quitter Donville-les-Bains et le super emplacement au bord de la place que j'avais trouvé. Je suis arrivé hier soir dans la nuit et je n'ai découvert le payasage que ce matin et j'ai bien aprécié.

![Donville-les-Bains vu du van](/img/Donville-les-Bains-vu-du-van.jpg)

J'ai commencer a travailler dans le van avant que le soleil se léve. Vers 9h j'ai enfin vu la mer et je suis allé boire mon thè sur la promenade.

![Donville-les-Bains Promenuade du matin](/img/Donville-les-Bains%20Promenuade%20du%20matin.jpg)
![Donville-les-Bains the du matin](/img/Donville-les-Bains%20the%20du%20matin.jpg)

Mais vers 10h mon ordinateur n'avais plus de batterie. Comme hier je me suis brancher sur la prise allume cigare lié a la batterie moteur. Surprise trés rapidement le voyant de batterie moteur c'est mis a clignoté sur l'ordinateur de bord (écran au dessus du frigo). Je suis super content que ce systéme marche aussi bien. Par contre je ne comprend pas pourquoi hier j'ai pu recharger complétement mon ordinateur sur la batterie moteur et ce matin non. J'ai allumé le chauffage ce matin peut être que ca joue sur la batterie ?

![Donville-les-Bains la promenage](/img/Donville-les-Bains%20la%20promenage.jpg)
![Donville-les-Bains les cabanes le long de la plage](/img/Donville-les-Bains%20les%20cabanes%20le%20long%20de%20la%20plage.jpg)

En tout cas je suis parti rapidement pour Coutance allez voir un coworking qui avait l'être sympa. Et bien ca avait toujours l'aire sympa mais bien sur pandémié oblige c'était fermé au publique. Donc j'ai continué jusqu'a une aire de camping car repéré précédament pas loin de la côte et avec électricité. J'y suis depuis et la côté de Agon-Coutainville est trés jolie !

![Agon-Coutainville bord de mer](/img/Agon-Coutainville%20bord%20de%20mer.jpg)

Ce midi j'ai pu profiter de ma pause pour faire quelque repérage et manger une salade sur la place. Je me suis aussi promené le long de la grand jeté. J'ai répérer une pizzeria qui fait a emporter donc ce soir pour soutenir le commerce local 😉 je vais me faire une pizza 🍕 et un film 🎬 sans doute.

![Agon-Coutainville la plage](/img/Agon-Coutainville%20la%20plage.jpg)
![Agon-Coutainville la mer](/img/Agon-Coutainville%20la%20mer.jpg)

Je ne pense pas que je vais pouvoir resté ici car malgrés que le coin soit sympa l'aire de camping car n'a pas de toilette et il n'y a pas de proche. C'est vraiment pas pratique.
