---
title: De Hauteville-sur-mer à Nantes
date: 2020-12-19T13:25:17.094Z
author: ArnaudBan
description: Fin de la semaine, je rentre à Nantes
tags:
  - normandie-2020
layout: layouts/post.njk
---
Dernière matinée à Hauteville-sur-mer. Je suis partie en fin de matiné direction Renne pour passer à Ikéa acheter de quoi rangé les légos des enfants.

![](/img/hauteville-escalier-sur-mer.jpg "Un escalier sur la mer")

Ensuite j'ai continué de ma pause midi étendu pour passer acheter le cadeau de Noël de Elijah à Maleville ( 30m de Nantes ). J'ai bossé sur  une aire de picnic pas super sympa à la sortie du village. C'est ca aussi la vanlife, des fois on a pas le temps de trouver un endroit sympa.

![](/img/malville-van.jpg "Aire de picnic de Malville")

Heureusement David m'a envoyé, sans savoir que c'est ce dont j'avais besoin, une bonne dose de groove avec en plus une anecdote fun. Il m'a dit d'écouter Vulfpeck et regarder leur histoire sur Wikipedia. Je vous conseil aussi d'écouter ce groupe et voici l'histoire fun derrière le groupe :

> En 2014, ils publient sur la plateforme [Spotify](https://fr.wikipedia.org/wiki/Spotify "Spotify") l'album *Sleepify*, ne comportant que des pistes entièrement silencieuses. Le groupe espère ainsi pouvoir financer une tournée en accès libre. En demandant à leurs auditeurs de jouer l'album en boucle pendant leur sommeil, le groupe reçoit ainsi de la part du site près de 20.000$, et concrétise son projet en septembre 2014 lors du Sleepify Tour[ 2](https://fr.wikipedia.org/wiki/Vulfpeck#cite_note-2)

<https://fr.wikipedia.org/wiki/Vulfpeck>
