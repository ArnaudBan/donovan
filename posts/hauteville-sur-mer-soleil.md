---
title: Hauteville-sur-mer sous le soleil
description: Journée à Hauteville-sur-mer sous le soleil
date: 2020-12-17
author: ArnaudBan
trip: normandie-2020
tags:
  - normandie-2020
layout: layouts/post.njk
---

Hier soir j'ai été me chercher un fish'n chips, avant 20h bien sûr pour respecter le couvre feu. Mais aprés mon repas j'ai eu envie d'aller au toilette et j'ai donc enfreint le couvre feu et je me suis promené dans les rues déserte et c'était jolie.

![hauteville-sur-mer-nuit](/img/hauteville-sur-mer-nuit.jpg)

L'avantage pour moi du couvre feu c'est que je ne suis pas dérangé par les bruits de circulation, il n'y en a aucun. Je doute de toute facon il y en ai là où je suis.

![hauteville-sur-mer-matin](/img/hauteville-sur-mer-matin.jpg)

![hauteville-sur-mer-maree-haute](/img/hauteville-sur-mer-maree-haute.jpg)

![hauteville-sur-mer-van](/img/hauteville-sur-mer-van.jpg)

Ce matin vers 10h le soleil c'est enfin décidé a sortir et chauffé. J'ai pu ouvrir le toit ce qui est beaucoup plus agréable.

![hauteville-sur-mer-selfie-soleil](/img/hauteville-sur-mer-selfie-soleil.jpg)

![hauteville-sur-mer-van-soleil](/img/hauteville-sur-mer-van-soleil.jpg)

Ce midi j'ai été faire des petites course et j'ai acheté un tuyaux d'arrosage pour remplir le réservoir d'eau du van. J'ai repéré un robinet sur la place du marché (là où sont les toilettes)

J'ai donc le réservoir d'eau plein ! C'était la première fois que je faisait le plein d'eau, c'est toujours Claire qui l'avais fait à Noirmoutier précédement.

![hauteville-sur-mer-plage-soleil](/img/hauteville-sur-mer-plage-soleil.jpg)

Cette aprés midi je me suis a nouveau branché sur la borne électrique de voiture pour charger mon ordinateur.

Ce soir j'ai enfin réussit a voir jolie couché de soleil sur la mer, c'est facile je suis a côté, je vois la mer de ma fenêtre de van.

![hauteville-sur-mer-couche-soleil](/img/hauteville-sur-mer-couche-soleil.jpg)

![hauteville-sur-mer-couche-soleil-2](/img/hauteville-sur-mer-couche-soleil-2.jpg)

![hauteville-sur-mer-couche-soleil-3](/img/hauteville-sur-mer-couche-soleil-3.jpg)

![hauteville-sur-mer-couche-soleil-fin-2](/img/hauteville-sur-mer-couche-soleil-fin-2.jpg)

![hauteville-sur-mer-couche-soleil-fin](/img/hauteville-sur-mer-couche-soleil-fin.jpg)
