---
title: Journée à Pont Chateau
description: Journée à Pont Chateau, rando et recharge gratuite
date: 2021-01-05
author: ArnaudBan
trip: pont-chateau-2020
tags:
  - pont-chateau-2020
layout: layouts/post.njk
---

C'est reparti pour un tour ! Le premier de 2021. Je suis parti lundi soir aprés avoir travaillé au bureau la journée. J'ai pris la direction d'une aire de camping car gratuite à Pont Chateau. On c'était déja arrété là avec les enfants quand on avait fait un week-end en camping car en septembre 2020.

Je suis arrivé il faisait nuit mais j'avais tout ce qu'il fallait pour me faire mes petites pâtes au pesto du soir.
![pont-chateau-soir.jpg](/img/pont-chateau-soir.jpg)

Le lendemain grace mate sous la couette. J'ai lancé le chauffage à 5h du mat car il faisait vraiment froid ( il fait 3° dehors ). Je me suis levé à 8H30 et je suis parti chercher une boulangerie. J'ai croisé une promenade qui avait l'aire sympa et le centre de la ville est mignon, j'ai donc décidé de resté aujourd'hui et faire la premenade a midi.

![pont-chateau-plan-balade.jpg](/img/pont-chateau-plan-balade.jpg)

![pont-chateau-cabane.jpg](/img/pont-chateau-cabane.jpg)


Problème mon mac qui a pourtant une batterie plein de veut pas démarer sans être relié au secteur, jamais rencontré ce problème. J'ai donc du trouvé un prise. Pauline, une collége qui a récament pris une voiture électrique m'a conseillé de chercher sur [chargemap](https://fr.chargemap.com/). J'y ai trouvé une prise proche et gratuite : le Leclerc Auto. J'ai donc été voir et le monsieur a l'accueil a été d'accord pour que je me branche. Pas super jolie comme coin mais ca marche pour bosser une heure.

Ensuite je suis revenu bosser un peu sur mon aire gratuite et a midi je suis partie faire la promenade. J'avais comme picnic un sandwitch au paté fait par maman, merci ma maman.

La promenade m'a réservé quelque surprise. Je me suis perdu dans un zone industrielle. Ensuite une partie du chemin était inondé et j'ai traversé la rivière sur le pond d'une voie rapide, mais a l'extérieur de la barrière car ca ma semblé moins dangereus que la voie d'arrêt d'urgence.

![pont-chateau-acces-centre.jpg](/img/pont-chateau-acces-centre.jpg)

![pont-chateau-sous-les-ponds.jpg](/img/pont-chateau-sous-les-ponds.jpg)

![pont-chateau-marais.jpg](/img/pont-chateau-marais.jpg)

![pont-chateau-chemin-inonde.jpg](/img/pont-chateau-chemin-inonde.jpg)

![pont-chateau-pond-traverse.jpg](/img/pont-chateau-pond-traverse.jpg)
