---
title: De Agon-Coutainville à Hauteville-sur-mer
description: Journée a la recherche du bon endroit et sous la pluie à Hauteville-sur-mer
date: 2020-12-16
author: ArnaudBan
tags:
  - normandie-2020
layout: layouts/post.njk
---
Hier soir j'ai bien été chercher ma pizza et je me suis regardé un petit film en la mangeant, c'était ma première soirée du couvre feu et c'était sympa comme programme.

J'étais garée dans une aire pour camping car propre et pratique. Le seul soucis c'est qu'il n'y avait pas de toillette a côté et ca c'est pas pratique. J'ai donc repris mes applications et mon smartphone et chercher un nouvel endroit au soleil, avec 4G, électricité et toilette.

Finalement je ne suis pas allez trés loin, je suis à Hauteville-sur-mer. Je suis garée juste a côté de la mer, j'ai une trés bonne 4G, il pleu mais c'est partout pareil. J'ai des toilettes publique pas trés loin.

![Hauteville-sur-mer la digue](/img/Hauteville-sur-mer-digue.jpg)
![Hauteville-sur-mer la plage](/img/Hauteville-sur-mer-plage.jpg)
![Hauteville-sur-mer la pluie](/img/Hauteville-sur-mer-pluie.jpg)

Pour l'electricité j'ai innové, j'ai recharger cette aprés midi mon ordi avec une borne pour voiture électrique. Je ne sais pas si j'ai vraiment droit...

![Hauteville-sur-mer recharge du van](/img/Hauteville-sur-mer-recharge-du-van.jpg)
![van-recharge electrique](/img/van-recharge-electrique.png)

Pour ce soir, la formule à emporter ma bien plu hier et je suis à côté d'un restau qui fait fish'n chips, je pense qu'une fois de plus il est de mon devoir de soutenir l'économie locale!

![Hauteville-sur-mer repas du soir](/img/Hauteville-sur-mer-repas-du-soir.jpg)
